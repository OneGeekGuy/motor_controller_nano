# Motor_Controller_NANO
 
![Picture 1](./MotorControllerNANO1.JPG)

## Description
The Motor Controller NANO has been designed as a shield for the well-known Arduino NANO which will allow you to control Motor Drivers focused on Stepper motors. The shield has also an LCD connection for a 16x2 LCD and Encoder + Switch connection for controlling the parameters of the Motor. Initially the Motor Controller NANO has been designed to work as Power-Feed for machines like Milling Machines, Lathes..., but can be modified to work as standard controller for a Motor as well. Not only HW design but also SW is completely open source so can be easily modified and adapted. 

The motor controller not only has the above-mentioned features, but also the possibility to add Right and Left endstops and an auxiliar input for different user defined purposes.

This device can control any Motor Driver which works with PULSE, DIRECTION and ENABLE signals, the voltage or drive strength required for these signals can be adapted, being by default prepared for 12V or 24V systems. 

As extra feature there are two auxiliary signals, one Output and the other one Input that can be used for different purposes. 

The Software which is provided by default and which you can find in the GIT  of this project, is prepared to be used as power feed, so the main function is to drive the motor at constant speed when the switch is in Right or Left Position. Endstops can be added, and the Motor Speed is adjusted using the Encoder, also certain parameters can be configurable to show in the screen the real feed rate in which the axis is moving. In case the user wants to develop new features or functionalities based on this Hardware, you can go to this document into section 6.0 Advanced Information to see a detailed description regarding which pins of the Arduino are used for which function. 

## Technical Specifications
* Input Voltage Range 12V to 24V.
* Power Consumption 0.4W
* Input Voltage Reverse Polarity Protection
* Arduino NANO Based
* ESD Protection for Encoder Inputs
* x3 Outputs for Motor Driver (EN, DIR and PULSE)
* Open Collector-Emitter Configuration
* Selectable 12V or 24V configuration
* x1 Direction Switch Input
* x2 Endstop Inputs (Right and Left)
* x1 Auxiliary Input
* x1 Auxiliary Output
* Open Collector-Emitter Configuration
* LCD Contrast Adjustment 
* Direct 16x2 LCD Connection
* Compact Design

## Project Structure
This project is completely open souce, and all the information; schematics, layout, software, manual...., can be found in this repository.

* [Schematics and Layout KiCad files](https://gitlab.com/OneGeekGuy/motor_controller_nano/-/tree/main/Hardware)  
* [User Manual](https://gitlab.com/OneGeekGuy/motor_controller_nano/-/blob/main/Motor_Controller_NANO_User_Manual.pdf)
* [Software](https://gitlab.com/OneGeekGuy/motor_controller_nano/-/tree/main/Software)

![Picture 1](./MotorControllerNANO2.JPG)
![Picture 1](./MotorControllerNANO3.JPG)
